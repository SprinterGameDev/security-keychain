package com.sprinter.keychain.repositories.source.preferences

interface SecurityPreferences : Preferences {

    companion object {

        val KEYSTORE_ALIAS_NAME = "KeyChain"

    }

}
